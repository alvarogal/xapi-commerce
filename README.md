# xapi-commerce

Trabajo de Fin de Grado para UNIR

**Descripción**
<br>
Módulo para Drupal 7 que integra Drupal y el módulo Commerce con la especificación xAPI.

**Autoría**

Alumno: Álvaro Galán Galindo
<br>
Módulo: Trabajo de Fin de Grado
<br>
Curso lectivo: 2018/2019
